var map, infobulle, autocomplete, input, Projection_En_Cours, t_Pays = [],
    nDec_src = 10,
    nDec_dest = 3,
    nDec_carte = 3,
    nDec_tr = 3,
    nDec_s = 6,
    unite_angle = "deg",
    nature_coords_src = "angle",
    nature_coords_dest = "distance",
    nature_coords_carte = "distance",
    nature_coords_centre = "distance",
    commande = "Coordonnees",
    Actions = [],
    Polylignes = [],
    Polygones = [],
    Marqueurs = [],
    Projections_NTF_France = "EPSG:27561 EPSG:27562 EPSG:27563 EPSG:27564 EPSG:27571 EPSG:27572 EPSG:27573 EPSG:27574 EPSG:4807",
    Projections_RGF93_France = "EPSG:3942 EPSG:3943 EPSG:3944 EPSG:3945 EPSG:3945 EPSG:3946 EPSG:3947 EPSG:3948 EPSG:3949 EPSG:3950 EPSG:2154 EPSG:4171",
    Proj4_src, Proj4_dest, DATUM_src = "autre",
    DATUM_dest = "autre",
    Pays_src, Pays_dest, Systeme_src, Systeme_dest, Grille = "";

function Modifier_Labels(c, b) {
    "longlat" == proj4.defs[c].projName ? ("src" == b && (nature_coords_src = "angle", nDec_src = 8, "rad" == unite_angle && (nDec_src = 10)), "dest" == b && (nature_coords_dest = "angle", nDec_dest = 8, "rad" == unite_angle && (nDec_dest = 10)), document.getElementById("Unite_coord1_" + b).innerHTML = "\u00b0", document.getElementById("Unite_coord2_" + b).innerHTML = "\u00b0", document.getElementById("label_coord1_" + b).innerHTML = "Long =", document.getElementById("label_coord2_" + b).innerHTML = "Lat =", document.getElementById("label_coord3_" +
        b).innerHTML = "h =", mef_unite(unite_angle)) : ("src" == b && (nature_coords_src = "distance", nDec_src = 3, mef_unite_dec_src()), "dest" == b && (nature_coords_dest = "distance", nDec_dest = 3, mef_unite_dec_dest()), document.getElementById("Unite_coord1_" + b).innerHTML = "m", document.getElementById("Unite_coord2_" + b).innerHTML = "m", document.getElementById("label_coord1_" + b).innerHTML = "x (E) =", document.getElementById("label_coord2_" + b).innerHTML = "y (N) =", document.getElementById("label_coord3_" + b).innerHTML = "h =", "EPSG:4326000" ==
        c && (document.getElementById("label_coord1_" + b).innerHTML = "X =", document.getElementById("label_coord2_" + b).innerHTML = "Y =", document.getElementById("label_coord3_" + b).innerHTML = "Z ="))
}

function mef_unite(c) {
    "deg" == c && ("angle" == nature_coords_src && (mef_unite_dec_src(), document.getElementById("Unite_coord1_src").innerHTML = "\u00b0", document.getElementById("Unite_coord2_src").innerHTML = "\u00b0", nDec_src = 8), "angle" == nature_coords_dest && (mef_unite_dec_dest(), document.getElementById("Unite_coord1_dest").innerHTML = "\u00b0", document.getElementById("Unite_coord2_dest").innerHTML = "\u00b0", nDec_dest = 8), unite_angle = "deg");
    "dms" == c && ("angle" == nature_coords_src && mef_unite_b60_src(), "angle" == nature_coords_dest &&
        mef_unite_b60_dest(), unite_angle = "dms");
    "grad" == c && ("angle" == nature_coords_src && (mef_unite_dec_src(), document.getElementById("Unite_coord1_src").innerHTML = "g", document.getElementById("Unite_coord2_src").innerHTML = "g", nDec_src = 8), "angle" == nature_coords_dest && (mef_unite_dec_dest(), document.getElementById("Unite_coord1_dest").innerHTML = "g", document.getElementById("Unite_coord2_dest").innerHTML = "g", nDec_dest = 8), unite_angle = "grad");
    "rad" == c && ("angle" == nature_coords_src && (mef_unite_dec_src(), document.getElementById("Unite_coord1_src").innerHTML =
        "r", document.getElementById("Unite_coord2_src").innerHTML = "r", nDec_src = 10), "angle" == nature_coords_dest && (mef_unite_dec_dest(), document.getElementById("Unite_coord1_dest").innerHTML = "r", document.getElementById("Unite_coord2_dest").innerHTML = "r", nDec_dest = 10), unite_angle = "rad")
}

function mef_unite_dec_src() {
    document.getElementById("div_coord2_Db60_src").style.visibility = "hidden";
    document.getElementById("div_coord2_dec_src").style.visibility = "visible";
    document.getElementById("div_coord1_Db60_src").style.visibility = "hidden";
    document.getElementById("div_coord1_dec_src").style.visibility = "visible"
}

function mef_unite_dec_dest() {
    document.getElementById("div_coord2_Db60_dest").style.visibility = "hidden";
    document.getElementById("div_coord2_dec_dest").style.visibility = "visible";
    document.getElementById("div_coord1_Db60_dest").style.visibility = "hidden";
    document.getElementById("div_coord1_dec_dest").style.visibility = "visible"
}

function mef_unite_b60_src() {
    document.getElementById("div_coord2_dec_src").style.visibility = "hidden";
    document.getElementById("div_coord2_Db60_src").style.visibility = "visible";
    document.getElementById("div_coord1_dec_src").style.visibility = "hidden";
    document.getElementById("div_coord1_Db60_src").style.visibility = "visible"
}

function mef_unite_b60_dest() {
    document.getElementById("div_coord2_dec_dest").style.visibility = "hidden";
    document.getElementById("div_coord2_Db60_dest").style.visibility = "visible";
    document.getElementById("div_coord1_dec_dest").style.visibility = "hidden";
    document.getElementById("div_coord1_Db60_dest").style.visibility = "visible"
}

function Definir_Projection(c, b) {
    if ("src" == b || "dest" == b) document.getElementById("convertir").disabled = !0;
    if ("undefined" == typeof proj4.defs[c]) {
        var e = "/lib/defs/" + c.replace(":", "") + ".txt";
        $.ajax({
            url: e,
            dataType: "text",
            cache: !1,
            success: function(d, a, e) {
                proj4.defs(c, d.trim());
                "src" == b ? Proj4_src = new proj4.Proj(c) : Proj4_dest = new proj4.Proj(c);
                Modifier_Labels(c, b);
                if ("src" == b || "dest" == b) document.getElementById("convertir").disabled = !1
            },
            error: function(d, a, e) {
                d = 0 < c.indexOf("ORG") ? "http://spatialreference.org/ref/sr-org/" +
                    c.replace("SR-ORG:", "") + "/proj4/" : "http://spatialreference.org/ref/epsg/" + c.replace("EPSG:", "") + "/proj4/";
                $.ajax({
                    url: "/processeur/Lire_Script_Externe.php",
                    data: { Adresse: d, Code: c.replace(":", "") },
                    dataType: "text",
                    cache: !1,
                    success: function(a, d, e) {
                        proj4.defs(c, a.trim());
                        "src" == b ? Proj4_src = new proj4.Proj(c) : Proj4_dest = new proj4.Proj(c);
                        Modifier_Labels(c, b);
                        if ("src" == b || "dest" == b) document.getElementById("convertir").disabled = !1
                    },
                    error: function(a, b, c) { alert("Probleme innattendu...") }
                })
            }
        })
    } else if ("src" ==
        b ? Proj4_src = new proj4.Proj(c) : Proj4_dest = new proj4.Proj(c), Modifier_Labels(c, b), "src" == b || "dest" == b) document.getElementById("convertir").disabled = !1;
    document.getElementById("div_Grille_France").style.display = "none";
    document.getElementById("check_Grille_France").checked = !1;
    document.getElementById("div_Grid_USA").style.display = "none";
    document.getElementById("check_Grid_USA").checked = !1;
    document.getElementById("div_Grid_CANADA").style.display = "none";
    document.getElementById("check_Grid_CANADA").checked = !1;
    document.getElementById("div_Grid_UK").style.display = "none";
    document.getElementById("check_Grid_UK").checked = !1;
    document.getElementById("div_Grid_Australia").style.display = "none";
    document.getElementById("check_Grid_Australia").checked = !1;
    document.getElementById("div_Grid_Deutschland").style.display = "none";
    document.getElementById("check_Grid_Deutschland").checked = !1;
    document.getElementById("div_Grid_Espana").style.display = "none";
    document.getElementById("check_Grid_Espana").checked = !1;
    document.getElementById("div_Grid_Portugal").style.display =
        "none";
    document.getElementById("check_Grid_Portugal").checked = !1;
    document.getElementById("div_Grid_Italia").style.display = "none";
    document.getElementById("check_Grid_Italia").checked = !1;
    Pays_src = document.getElementById("Pays_src").value;
    Pays_dest = document.getElementById("Pays_dest").value;
    Systeme_src = document.getElementById("Projections_src").value;
    Systeme_dest = document.getElementById("Projections_dest").value;
    if ("France" == Pays_src && "France" == Pays_dest && (DATUM_dest = DATUM_src = "autre", -1 < Projections_NTF_France.indexOf(Systeme_src) &&
            (DATUM_src = "NTF"), -1 < Projections_NTF_France.indexOf(Systeme_dest) && (DATUM_dest = "NTF"), -1 < Projections_RGF93_France.indexOf(Systeme_src) && (DATUM_src = "RGF93"), -1 < Projections_RGF93_France.indexOf(Systeme_dest) && (DATUM_dest = "RGF93"), "NTF" == DATUM_src && "RGF93" == DATUM_dest || "RGF93" == DATUM_src && "NTF" == DATUM_dest)) {
        document.getElementById("div_Grille_France").style.display = "block";
        document.getElementById("check_Grille_France").checked = !0;
        return
    }
    if ("USA" == Pays_src && "USA" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        var d = document.getElementById("Projections_src"),
            e = document.getElementById("Projections_dest"),
            d = d.options[d.selectedIndex].text,
            e = e.options[e.selectedIndex].text; - 1 < d.indexOf("NAD27") && (DATUM_src = "NAD27");
        if (-1 < d.indexOf("NAD83") || -1 < d.indexOf("WGS 84")) DATUM_src = "NAD83"; - 1 < e.indexOf("NAD27") && (DATUM_dest = "NAD27");
        if (-1 < e.indexOf("NAD83") || -1 < e.indexOf("WGS 84")) DATUM_dest = "NAD83";
        if ("NAD27" == DATUM_src && "NAD83" == DATUM_dest || "NAD83" == DATUM_src && "NAD27" == DATUM_dest) {
            document.getElementById("div_Grid_USA").style.display =
                "block";
            document.getElementById("check_Grid_USA").checked = !0;
            return
        }
    }
    if ("Canada" == Pays_src && "Canada" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("NAD27") && (DATUM_src = "NAD27");
        if (-1 < d.indexOf("NAD83") || -1 < d.indexOf("WGS 84")) DATUM_src = "NAD83"; - 1 < e.indexOf("NAD27") && (DATUM_dest = "NAD27");
        if (-1 < e.indexOf("NAD83") || -1 < e.indexOf("WGS 84")) DATUM_dest =
            "NAD83";
        if ("NAD27" == DATUM_src && "NAD83" == DATUM_dest || "NAD83" == DATUM_src && "NAD27" == DATUM_dest) {
            document.getElementById("div_Grid_CANADA").style.display = "block";
            document.getElementById("check_Grid_CANADA").checked = !0;
            return
        }
    }
    if ("UK" == Pays_src && "UK" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text;
        if (-1 < d.indexOf("OS") || -1 < d.toUpperCase().indexOf("IR")) DATUM_src =
            "OSGB";
        if (-1 < d.indexOf("WGS") || -1 < d.indexOf("ETRS")) DATUM_src = "WGS84";
        if (-1 < e.indexOf("OS") || -1 < e.toUpperCase().indexOf("IR")) DATUM_dest = "OSGB";
        if (-1 < e.indexOf("WGS") || -1 < e.indexOf("ETRS")) DATUM_dest = "WGS84";
        if ("OSGB" == DATUM_src && "WGS84" == DATUM_dest || "WGS84" == DATUM_src && "OSGB" == DATUM_dest) {
            document.getElementById("div_Grid_UK").style.display = "block";
            document.getElementById("check_Grid_UK").checked = !0;
            return
        }
    }
    if ("Australia" == Pays_src && "Australia" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("AGD66") && (DATUM_src = "AGD66"); - 1 < d.indexOf("AGD84") && (DATUM_src = "AGD84");
        if (-1 < d.indexOf("WGS") || -1 < d.indexOf("GDA")) DATUM_src = "GDA"; - 1 < e.indexOf("AGD66") && (DATUM_dest = "AGD66"); - 1 < e.indexOf("AGD84") && (DATUM_dest = "AGD84");
        if (-1 < e.indexOf("WGS") || -1 < e.indexOf("GDA")) DATUM_dest = "GDA";
        if (-1 < DATUM_src.indexOf("AGD") && "GDA" == DATUM_dest || "GDA" == DATUM_src && -1 < DATUM_dest.indexOf("AGD")) {
            document.getElementById("div_Grid_Australia").style.display =
                "block";
            document.getElementById("check_Grid_Australia").checked = !0;
            return
        }
    }
    if ("Deutschland" == Pays_src && "Deutschland" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("DHDN") && (DATUM_src = "DHDN");
        if (-1 < d.indexOf("WGS") || -1 < d.indexOf("ETRS89")) DATUM_src = "ETRS89"; - 1 < e.indexOf("DHDN") && (DATUM_dest = "DHDN");
        if (-1 < e.indexOf("WGS") || -1 < e.indexOf("ETRS89")) DATUM_dest =
            "ETRS89";
        if ("DHDN" == DATUM_src && "ETRS89" == DATUM_dest || "ETRS89" == DATUM_src && "DHDN" == DATUM_dest) {
            document.getElementById("div_Grid_Deutschland").style.display = "block";
            document.getElementById("check_Grid_Deutschland").checked = !0;
            return
        }
    }
    if ("Espana" == Pays_src && "Espana" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("ED50") && (DATUM_src = "ED50");
        if (-1 < d.indexOf("WGS 84") || -1 < d.indexOf("ETRS89")) DATUM_src = "ETRS89"; - 1 < e.indexOf("ED50") && (DATUM_dest = "ED50");
        if (-1 < e.indexOf("WGS 84") || -1 < e.indexOf("ETRS89")) DATUM_dest = "ETRS89";
        if ("ED50" == DATUM_src && "ETRS89" == DATUM_dest || "ETRS89" == DATUM_src && "ED50" == DATUM_dest) {
            document.getElementById("div_Grid_Espana").style.display = "block";
            document.getElementById("check_Grid_Espana").checked = !0;
            return
        }
    }
    if ("Portugal" == Pays_src && "Portugal" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("Datum 73") && (DATUM_src = "Datum73"); - 1 < d.indexOf("Lisbon") && (DATUM_src = "Lisbon");
        if (-1 < d.indexOf("WGS 84") || -1 < d.indexOf("ETRS89")) DATUM_src = "ETRS89"; - 1 < e.indexOf("Datum 73") && (DATUM_dest = "Datum73"); - 1 < e.indexOf("Lisbon") && (DATUM_dest = "Lisbon");
        if (-1 < e.indexOf("WGS 84") || -1 < e.indexOf("ETRS89")) DATUM_dest = "ETRS89";
        if ("Datum73" == DATUM_src && "ETRS89" == DATUM_dest || "ETRS89" == DATUM_src &&
            "Datum73" == DATUM_dest || "Lisbon" == DATUM_src && "ETRS89" == DATUM_dest || "ETRS89" == DATUM_src && "Lisbon" == DATUM_dest) {
            document.getElementById("div_Grid_Portugal").style.display = "block";
            document.getElementById("check_Grid_Portugal").checked = !0;
            return
        }
    }
    if ("Italie" == Pays_src && "Italie" == Pays_dest) {
        DATUM_dest = DATUM_src = "autre";
        d = document.getElementById("Projections_src");
        e = document.getElementById("Projections_dest");
        d = d.options[d.selectedIndex].text;
        e = e.options[e.selectedIndex].text; - 1 < d.indexOf("Monte") && (DATUM_src =
            "Roma40"); - 1 < d.indexOf("ED50") && (DATUM_src = "ED50");
        if (-1 < d.indexOf("WGS 84") || -1 < d.indexOf("ETRS89")) DATUM_src = "WGS84"; - 1 < e.indexOf("Monte") && (DATUM_dest = "Roma40"); - 1 < e.indexOf("ED50") && (DATUM_dest = "ED50");
        if (-1 < e.indexOf("WGS 84") || -1 < e.indexOf("ETRS89")) DATUM_dest = "WGS84";
        if ("Roma40" == DATUM_src && "WGS84" == DATUM_dest || "WGS84" == DATUM_src && "Roma40" == DATUM_dest) document.getElementById("div_Grid_Italia").style.display = "block", document.getElementById("check_Grid_Italia").checked = !0, document.getElementById("Grigliati_Italia").innerHTML =
            "Grigliati NTv2: Roma40 - WGS84", Grille = "Roma40_WGS84";
        else if ("ED50" == DATUM_src && "WGS84" == DATUM_dest || "WGS84" == DATUM_src && "ED50" == DATUM_dest) document.getElementById("div_Grid_Italia").style.display = "block", document.getElementById("check_Grid_Italia").checked = !0, document.getElementById("Grigliati_Italia").innerHTML = "Grigliati NTv2: ED50 - WGS84", Grille = "ED50_WGS84";
        else if ("Roma40" == DATUM_src && "ED50" == DATUM_dest || "ED50" == DATUM_src && "Roma40" == DATUM_dest) document.getElementById("div_Grid_Italia").style.display =
            "block", document.getElementById("check_Grid_Italia").checked = !0, document.getElementById("Grigliati_Italia").innerHTML = "Grigliati NTv2: Roma40 - ED50", Grille = "Roma40_ED50"
    }
}

function Inserer_Menu_Projections(c, b) { c(b) }

function Modifier_Liste_Projections(c, b, e) {
    if ("src" == e || "dest" == e) document.getElementById("convertir").disabled = !0;
    var d = document.getElementById(b);
    for (b = d.length - 1; 0 <= b; b--) d.remove(b);
    1 != t_Pays[c] ? $.ajax({
        url: "/Projections-Pays/" + c + ".js",
        dataType: "script",
        cache: !1,
        success: function(b, a, m) {
            b = eval("Menu_Projections_" + c);
            Inserer_Menu_Projections(b, d);
            t_Pays[c] = !0;
            Definir_Projection(d.options[d.selectedIndex].value, e)
        },
        error: function(b, a, c) { alert("Probleme innattendu") }
    }) : (b = eval("Menu_Projections_" +
        c), Inserer_Menu_Projections(b, d), Definir_Projection(d.options[d.selectedIndex].value, e))
}

function Afficher_Infos_Systeme(c) {
    c = document.getElementById(c);
    c = c.options[c.selectedIndex].value;
    c = 0 < c.indexOf("ORG") ? "http://spatialreference.org/ref/sr-org/" + c.replace("SR-ORG:", "") + "/html/" : "http://spatialreference.org/ref/epsg/" + c.replace("EPSG:", "") + "/html/";
    window.open(c)
}

function Definir_Systeme_NTF_Grille(c, b, e) {
    c = [c, b, e];
    c = proj4(Proj4_src, proj4.defs.WGS84).forward(c);
    if ("NTF" == DATUM_src) var d = new proj4.Proj(Systeme_src);
    "NTF" == DATUM_dest && (d = new proj4.Proj(Systeme_dest));
    var f = !1;
    $.ajax({
        url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
        data: { lng: c[0], lat: c[1], pays: "France" },
        cache: !1,
        async: !1,
        success: function(a, b, c) {
            "Hors zone de grille" != a.trim() ? (d.datum.datum_params = a.trim().split(",").map(function(a) { return parseFloat(a) }), d.datum_params = d.datum.datum_params,
                f = !0) : f = !1
        },
        error: function(a, b, c) { alert("Probleme innattendu...") }
    });
    return 1 == f ? d : "Hors zone de grille"
}

function Convertir(c, b, e, d, f) {
    to_Meter_src = 1;
    "undefined" != typeof Proj4_src.to_meter && (to_Meter_src = Proj4_src.to_meter);
    to_Meter_dest = 1;
    "undefined" != typeof Proj4_dest.to_meter && (to_Meter_dest = Proj4_dest.to_meter);
    c /= to_Meter_src;
    b /= to_Meter_src;
    e /= to_Meter_src;
    if ("EPSG:4326000" == d) return d = 3.141592653589793, b = new proj4.toPoint([c, b, e]), c = Proj4_src.datum, c.geocentric_to_geodetic(b), b.x = 180 * b.x / d, b.y = 180 * b.y / d, b;
    if ("EPSG:4326000" == f) return d = 3.141592653589793, b = new proj4.toPoint([c * d / 180, b * d / 180, e]),
        c = Proj4_src.datum, c.geodetic_to_geocentric(b), b;
    var a = new proj4.toPoint([c, b, e]);
    d = !1;
    if (1 == document.getElementById("check_Grid_USA").checked || 1 == document.getElementById("check_Grid_CANADA").checked) d = !0;
    if (1 == d) {
        var m = new proj4.Proj("EPSG:4267"),
            u = new proj4.Proj("EPSG:4269");
        c = new proj4.toPoint([c, b, e]);
        c = proj4(Proj4_src, m).forward(c);
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: c.x, lat: c.y, pays: Pays_src },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" ==
                b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "NAD27" == DATUM_src && "NAD83" == DATUM_dest && (a = proj4(Proj4_src, m).forward(a), a.x -= c, a.y += b, a = proj4(u, Proj4_dest).forward(a)), "NAD83" == DATUM_src && "NAD27" == DATUM_dest && (a = proj4(Proj4_src, u).forward(a), a.x += c, a.y -= b, a = proj4(m, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_UK").checked;
    if (1 == d) {
        var n = new proj4.Proj("EPSG:4277"),
            k = new proj4.Proj("WGS84");
        c = new proj4.toPoint([c, b, e]);
        c = proj4(Proj4_src, n).forward(c);
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: c.x, lat: c.y, pays: "UK" },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "OSGB" == DATUM_src && "WGS84" == DATUM_dest && (a = proj4(Proj4_src, n).forward(a), a.x -= c, a.y += b, a = proj4(k, Proj4_dest).forward(a)), "WGS84" == DATUM_src && "OSGB" == DATUM_dest && (a = proj4(Proj4_src,
                    k).forward(a), a.x += c, a.y -= b, a = proj4(n, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_Australia").checked;
    if (1 == d) {
        var p = new proj4.Proj("EPSG:4203"),
            v = new proj4.Proj("EPSG:4939");
        c = new proj4.toPoint([c, b, e]);
        c = proj4(Proj4_src, p).forward(c);
        b = "AGD66";
        if ("AGD84" == DATUM_src || "AGD84" == DATUM_dest) b = "AGD84";
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: c.x, lat: c.y, pays: "Australia", Version_AGD: b },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) { "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "AGD66" != DATUM_src && "AGD84" != DATUM_src || "GDA" != DATUM_dest || (a = proj4(Proj4_src, p).forward(a), a.x -= c, a.y += b, a = proj4(v, Proj4_dest).forward(a)), "GDA" != DATUM_src || "AGD66" != DATUM_dest && "AGD84" != DATUM_dest || (a = proj4(Proj4_src, v).forward(a), a.x += c, a.y -= b, a = proj4(p, Proj4_dest).forward(a))) },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_Deutschland").checked;
    if (1 == d) {
        var q = new proj4.Proj("EPSG:4314"),
            g = new proj4.Proj("EPSG:4258");
        c = new proj4.toPoint([c, b, e]);
        c = proj4(Proj4_src, q).forward(c);
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: c.x, lat: c.y, pays: "Deutschland" },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c =
                    b[1], b = b[2], "DHDN" == DATUM_src && "ETRS89" == DATUM_dest && (a = proj4(Proj4_src, q).forward(a), a.x -= c, a.y += b, a = proj4(g, Proj4_dest).forward(a)), "ETRS89" == DATUM_src && "DHDN" == DATUM_dest && (a = proj4(Proj4_src, g).forward(a), a.x += c, a.y -= b, a = proj4(q, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_Espana").checked;
    if (1 == d) {
        var h = new proj4.Proj("EPSG:4230"),
            g = new proj4.Proj("EPSG:4258");
        c = new proj4.toPoint([c, b, e]);
        c = proj4(Proj4_src, h).forward(c);
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: c.x, lat: c.y, pays: "Espana" },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "ED50" == DATUM_src && "ETRS89" == DATUM_dest && (a = proj4(Proj4_src, h).forward(a), a.x -= c, a.y += b, a = proj4(g, Proj4_dest).forward(a)), "ETRS89" == DATUM_src && "ED50" == DATUM_dest && (a = proj4(Proj4_src, g).forward(a),
                    a.x += c, a.y -= b, a = proj4(h, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_Portugal").checked;
    if (1 == d) {
        var r = new proj4.Proj("EPSG:4274"),
            t = new proj4.Proj("EPSG:4803"),
            g = new proj4.Proj("EPSG:4258");
        b = new proj4.toPoint([c, b, e]);
        b = "Datum73" == DATUM_src || "Datum73" == DATUM_dest ? proj4(Proj4_src, r).forward(b) : proj4(Proj4_src, t).forward(b);
        c = "Datum73";
        if ("Lisbon" == DATUM_src ||
            "Lisbon" == DATUM_dest) c = "Lisbon";
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: b.x, lat: b.y, pays: "Portugal", Datum73_ou_Lisbon: c },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "Datum73" == DATUM_src && "ETRS89" == DATUM_dest && (a = proj4(Proj4_src, r).forward(a), a.x -= c, a.y += b, a = proj4(g, Proj4_dest).forward(a)), "Lisbon" == DATUM_src && "ETRS89" == DATUM_dest && (a = proj4(Proj4_src, t).forward(a),
                    a.x -= c, a.y += b, a = proj4(g, Proj4_dest).forward(a)), "ETRS89" == DATUM_src && "Datum73" == DATUM_dest && (a = proj4(Proj4_src, g).forward(a), a.x += c, a.y -= b, a = proj4(r, Proj4_dest).forward(a)), "ETRS89" == DATUM_src && "Lisbon" == DATUM_dest && (a = proj4(Proj4_src, g).forward(a), a.x += c, a.y -= b, a = proj4(t, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grid_Italia").checked;
    if (1 == d) {
        var l = new proj4.Proj("EPSG:4265");
        new proj4.Proj("EPSG:4806");
        h = new proj4.Proj("EPSG:4230");
        k = new proj4.Proj("WGS84");
        b = new proj4.toPoint([c, b, e]);
        b = "Roma40" == DATUM_src || "Roma40" == DATUM_dest ? proj4(Proj4_src, l).forward(b) : proj4(Proj4_src, h).forward(b);
        $.ajax({
            url: "/processeur/conversion-coordonnees/Utiliser_Grille.php",
            data: { lng: b.x, lat: b.y, pays: "Italia", Grille: Grille },
            dataType: "json",
            cache: !1,
            async: !1,
            success: function(b, c, d) {
                "Hors zones de grilles" == b[1] ? a = proj4(Proj4_src, Proj4_dest).forward(a) : (c = b[1], b = b[2], "Roma40" == DATUM_src &&
                    "WGS84" == DATUM_dest && (a = proj4(Proj4_src, l).forward(a), a.x -= c, a.y += b, a = proj4(k, Proj4_dest).forward(a)), "ED50" == DATUM_src && "WGS84" == DATUM_dest && (a = proj4(Proj4_src, h).forward(a), a.x -= c, a.y += b, a = proj4(k, Proj4_dest).forward(a)), "WGS84" == DATUM_src && "Roma40" == DATUM_dest && (a = proj4(Proj4_src, k).forward(a), a.x += c, a.y -= b, a = proj4(l, Proj4_dest).forward(a)), "WGS84" == DATUM_src && "ED50" == DATUM_dest && (a = proj4(Proj4_src, k).forward(a), a.x += c, a.y -= b, a = proj4(h, Proj4_dest).forward(a)), "Roma40" == DATUM_src && "ED50" == DATUM_dest &&
                    (a = proj4(Proj4_src, l).forward(a), a.x -= c, a.y += b, a = proj4(h, Proj4_dest).forward(a)), "ED50" == DATUM_src && "Roma40" == DATUM_dest && (a = proj4(Proj4_src, h).forward(a), a.x += c, a.y -= b, a = proj4(l, Proj4_dest).forward(a)))
            },
            error: function(a, b, c) { alert("Probleme innattendu...") }
        });
        a.x *= to_Meter_dest;
        a.y *= to_Meter_dest;
        a.z *= to_Meter_dest;
        return a
    }
    d = document.getElementById("check_Grille_France").checked;
    if (1 == d) return c = Definir_Systeme_NTF_Grille(c, b, e), "Hors zone de grille" != c ? "NTF" == DATUM_src ? (a = proj4(c, Proj4_dest).forward(a),
        Proj4_src.datum.datum_params = Proj4_src.datum_params) : (a = proj4(Proj4_src, c).forward(a), Proj4_dest.datum.datum_params = Proj4_dest.datum_params) : a = proj4(Proj4_src, Proj4_dest).forward(a), a.x *= to_Meter_dest, a.y *= to_Meter_dest, a.z *= to_Meter_dest, a;
    a = proj4(Proj4_src, Proj4_dest).forward(a);
    a.x *= to_Meter_dest;
    a.y *= to_Meter_dest;
    a.z *= to_Meter_dest;
    return a
}

function Conversion() {
    if ("angle" == nature_coords_src && "dms" == unite_angle) {
        var c = [],
            b = document.getElementById("Long_Db60_d_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        c.d = parseFloat(b.value);
        b = document.getElementById("Long_Db60_m_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        c.m = parseFloat(b.value);
        b = document.getElementById("Long_Db60_s_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        c.s = parseFloat(b.value);
        var e = [],
            b = document.getElementById("Lat_Db60_d_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        e.d = parseFloat(b.value);
        b = document.getElementById("Lat_Db60_m_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        e.m = parseFloat(b.value);
        b = document.getElementById("Lat_Db60_s_src");
        "" == b.value.replace(/\s/g, "") && (b.value =
            0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        e.s = parseFloat(b.value);
        if (60 <= c.m || 60 <= c.s || 60 <= e.m || 60 <= e.s) { alert("Erreur de saisie !"); return }
        if (0 > c.m || 0 > c.s || 0 > e.m || 0 > e.s) { alert("Erreur de saisie !"); return }
    } else {
        b = document.getElementById("Coord1_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) { alert("Erreur de saisie !"); return }
        b = 319438;
        var d = parseFloat(b.value),
            b = document.getElementById("Coord2_src");
        "" == b.value.replace(/\s/g, "") && (b.value = 0);
        if (isNaN(b.value)) {
            alert("Erreur de saisie !");
            return
        }
        b = 324725;
        var f = parseFloat(b.value)
    }
    "angle" == nature_coords_src && ("dms" == unite_angle && (d = dms_deg(c), f = dms_deg(e)), "grad" == unite_angle && (d = grad_deg(d), f = grad_deg(f)), "rad" == unite_angle && (d = rad_deg(d), f = rad_deg(f)));
    b = document.getElementById("texte_h_src");
    "" == b.value.replace(/\s/g, "") && (b.value = 0);
    if (isNaN(b.value)) alert("Erreur de saisie !");
    else {
        var b = parseFloat(b.value),
            b = 0.00;
        a = document.getElementById("Projections_src");
        Projection_src = a.options[a.selectedIndex].value;
        a = document.getElementById("Projections_dest");
        Projection_dest = a.options[a.selectedIndex].value;
        Projection_src = "EPSG:26191";
        Projection_dest = "EPSG:4979";
        b = Convertir(d, f, b, Projection_src, Projection_dest);
        d = b.x;
        f = b.y;
        console.log("NEW X:" + d + " NEW Y:" + f);
        "angle" == nature_coords_dest && ("dms" == unite_angle && (c = deg_dms(d, nDec_s), e = deg_dms(f, nDec_s)), "grad" == unite_angle && (d = deg_grad(d), f = deg_grad(f)), "rad" == unite_angle && (d = deg_rad(d), f = deg_rad(f)));
        "angle" == nature_coords_dest && "dms" == unite_angle ? (document.getElementById("Long_Db60_d_dest").value = c.d, document.getElementById("Long_Db60_m_dest").value = c.m, document.getElementById("Long_Db60_s_dest").value =
            c.s.toFixed(6), document.getElementById("Lat_Db60_d_dest").value = e.d, document.getElementById("Lat_Db60_m_dest").value = e.m, document.getElementById("Lat_Db60_s_dest").value = e.s.toFixed(6)) : (document.getElementById("Coord1_dest").value = d.toFixed(nDec_dest), document.getElementById("Coord2_dest").value = f.toFixed(nDec_dest));
        document.getElementById("texte_h_dest").value = b.z.toFixed(3)
    }
}

function Initialisation_Generale() {
    t_Pays.Mondial = !0;
    t_Pays.Maroc = !0;
    t_Pays.France = !0;
    var c = document.getElementById("Pays_src");
    c.selectedIndex = 0;
    c = document.getElementById("Pays_dest");
    c.selectedIndex = 0;
    proj4.defs("EPSG:4267", "+proj=longlat +ellps=clrk66 +datum=NAD27 +no_defs");
    proj4.defs("EPSG:4269", "+proj=longlat +ellps=GRS80 +datum=NAD83 +no_defs");
    proj4.defs("EPSG:4277", "+proj=longlat +ellps=airy +datum=OSGB36 +no_defs");
    proj4.defs("EPSG:4258", "+proj=longlat +ellps=GRS80 +no_defs");
    proj4.defs("EPSG:4230",
        "+proj=longlat +ellps=intl +no_defs");
    proj4.defs("EPSG:4203", "+proj=longlat +ellps=aust_SA +no_defs");
    proj4.defs("EPSG:4939", "+proj=longlat +ellps=GRS80 +towgs84=0.0,0.0,0.0,0.0,0.0,0.0,0.0 +no_defs");
    proj4.defs("EPSG:4314", "+proj=longlat +ellps=bessel +datum=potsdam +no_defs");
    proj4.defs("EPSG:4274", "+proj=longlat +ellps=intl +no_defs");
    proj4.defs("EPSG:4803", "+proj=longlat +ellps=intl +pm=lisbon +no_defs");
    proj4.defs("EPSG:4806", "+proj=longlat +ellps=intl +pm=rome +no_defs");
    proj4.defs("EPSG:4265",
        "+proj=longlat +ellps=intl +no_defs");
    for (var c = document.getElementById("Projections_src"), b = c.length - 1; 0 <= b; b--) c.remove(b);
    Inserer_Menu_Projections(Menu_Projections_Mondial, c);
    c.selectedIndex = 0;
    c = document.getElementById("Projections_dest");
    for (b = c.length - 1; 0 <= b; b--) c.remove(b);
    Inserer_Menu_Projections(Menu_Projections_Mondial, c);
    c.selectedIndex = 37;
    document.getElementById("Radio_deg").checked = !0;
    Menu_Proj = document.getElementById("Projections_src");
    Definir_Projection(Menu_Proj.options[Menu_Proj.selectedIndex].value,
        "src");
    Menu_Proj = document.getElementById("Projections_dest");
    Definir_Projection(Menu_Proj.options[Menu_Proj.selectedIndex].value, "dest")
}

function Initialisation() {
    Initialisation_liste_pays();
    Initialisation_Generale();
    document.getElementById("contenu").style.visibility = "visible";
    document.getElementById("load-page").style.display = "none"
}

function Envoyer_Fichier() {
    try {
        if (208E3 < this.files[0].size) {
            alert("Please use files whose size < 200 KB.");
            document.getElementById("Formulaire_Echange").reset();
            return
        }
    } catch (e) {}
    document.getElementById("Lien_Resultat").innerHTML = "Veuillez patienter...";
    var c = "Non";
    1 == document.getElementById("check_Grille_France").checked && (c = "France");
    1 == document.getElementById("check_Grid_USA").checked && (c = "USA");
    1 == document.getElementById("check_Grid_CANADA").checked && (c = "CANADA");
    1 == document.getElementById("check_Grid_UK").checked &&
        (c = "UK");
    1 == document.getElementById("check_Grid_Australia").checked && (c = "Australia");
    1 == document.getElementById("check_Grid_Deutschland").checked && (c = "Deutschland");
    1 == document.getElementById("check_Grid_Espana").checked && (c = "Espana");
    1 == document.getElementById("check_Grid_Portugal").checked && (c = "Portugal");
    1 == document.getElementById("check_Grid_Italia").checked && (c = "Italia");
    if ("Non" == c) document.getElementById("Formulaire_Echange").action = "/processeur/conversion-coordonnees/Transformer_Fichier.php?Utiliser_Grille=Non";
    else {
        var b = document.getElementById("Projections_src");
        Projection_src = b.options[b.selectedIndex].value;
        b = document.getElementById("Projections_dest");
        Projection_dest = b.options[b.selectedIndex].value;
        c = "?src=" + Projection_src + "&dest=" + Projection_dest + "&DATUM_src=" + DATUM_src + "&DATUM_dest=" + DATUM_dest + "&Utiliser_Grille=" + c;
        document.getElementById("Formulaire_Echange").action = "/processeur/conversion-coordonnees/Transformer_Fichier.php" + c
    }
    document.getElementById("Formulaire_Echange").submit()
};