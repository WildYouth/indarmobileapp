var res = localStorage.getItem('listeagences');
var listeAgences = JSON.parse(res);
// var listeAgences = res;

var selectAgence = '  <select name="select" class="form-control input-lg" id="IdselectAgence" onchange="ChangeItemAgence(this)">';
selectAgence = selectAgence + '<option value="F" selected>Choisir une agence</option>';
for (j = 0; j < listeAgences.agences.length; j++) {
    selectAgence = selectAgence + '<option value="' + j + '">' + listeAgences.agences[j].name + '</option> ';
}

selectAgence = selectAgence + '</select>';
document.getElementById("selectAgence").innerHTML = selectAgence;

function ChangeItemAgence(s) {

    var selectedItem = s[s.selectedIndex].value;
    if (selectedItem != "F") {
        var IdAgence = parseInt(s[s.selectedIndex].value);
        var selectBassin = '  <select name="select" class="form-control input-lg" id="IdselectBassin" onchange="ChangeItemBAssin(this)">';
        selectBassin = selectBassin + '<option value="F" selected>Choisir un bassin</option>';
        for (j = 0; j < listeAgences.agences[IdAgence].bassins.length; j++) {
            selectBassin = selectBassin + '<option value="' + IdAgence + '-' + j + '">' + listeAgences.agences[IdAgence].bassins[j].name + '</option> ';
        }
        selectBassin = selectBassin + '</select>';
        document.getElementById("selectBassin").innerHTML = selectBassin;

    } else {

        document.getElementById("selectBassin").innerHTML = "";
        document.getElementById("selectSousBassin").innerHTML = "";
        document.getElementById("selectStations").innerHTML = "";
        document.getElementById("dataStations").innerHTML = "";


    }


}

function ChangeItemBAssin(s) {


    var selectedItem = s[s.selectedIndex].value;
    if (selectedItem != "F") {
        var IdBassin = parseInt(selectedItem.split("-")[1]);
        var IdAgence = parseInt(selectedItem.split("-")[0]);
        var selectSousBassin = '  <select name="select" class="form-control input-lg" onchange="ChangeItemSousBassin(this)">';
        selectSousBassin = selectSousBassin + '<option value="F" selected>Choisir un sousbassin</option>';
        for (j = 0; j < listeAgences.agences[IdAgence].bassins[IdBassin].SousBassins.length; j++) {
            selectSousBassin = selectSousBassin + '<option value="' + IdAgence + '-' + IdBassin + '-' + j + '">' + listeAgences.Agences[IdAgence].Bassins[IdBassin].SousBassins[j].name + '</option> ';

        }
        selectSousBassin = selectSousBassin + '</select>';
        document.getElementById("selectSousBassin").innerHTML = selectSousBassin;
    } else {
        document.getElementById("selectSousBassin").innerHTML = "";
        document.getElementById("selectStations").innerHTML = "";
        document.getElementById("dataStations").innerHTML = "";
    }


}


function ChangeItemSousBassin(s) {

    var selectedItem = s[s.selectedIndex].value;
    if (selectedItem != "F") {
        var IdAgence = parseInt(selectedItem.split("-")[0]);
        var IdBassin = parseInt(selectedItem.split("-")[1]);
        var IdSousBassin = parseInt(selectedItem.split("-")[2]);

        var SelectTypeStation = '<select name="select" class="form-control input-lg" onchange="ChargementdesStations(this)">' +
            '<option value="">Choisir le type des stations</option>' +
            '<option value="' + IdAgence + '-' + IdBassin + '-' + IdSousBassin + '-0">Toutes les stations</option>' +
            '<option value="' + IdAgence + '-' + IdBassin + '-' + IdSousBassin + '-1">Les stations critiques</option>' +
            '<option value="' + IdAgence + '-' + IdBassin + '-' + IdSousBassin + '-2">Les stations non critiques</option>' +
            '</select>';

        document.getElementById("selectStations").innerHTML = SelectTypeStation;
        var URLStations = "ListeStations.html";
    } else {
        document.getElementById("selectStations").innerHTML = "";
        document.getElementById("dataStations").innerHTML = "";

    }

}

function ChargementdesStations(s) {


    var selectedItem = s[s.selectedIndex].value;
    var posAg = parseInt(selectedItem.split("-")[0]);
    var posBas = parseInt(selectedItem.split("-")[1]);
    var posSousBas = parseInt(selectedItem.split("-")[2]);
    var IdChoixStations = parseInt(selectedItem.split("-")[3]);

    window.location = 'AccesStations.html?idAgence=' + posAg + '&idBassin=' + posBas + '&idSousbassin=' + posSousBas + '&idchoix=' + IdChoixStations;



}

function chargementStations() {
    var parametrestab = location.search.split('?'); //"Agence2"; //"ABHBC";
    var parametres = parametrestab[1].split('&');
    var posAg = parametres[0].split('=')[1];
    var posBas = parametres[1].split('=')[1];
    var posSousBas = parametres[2].split('=')[1];
    var IdChoixStations = parametres[3].split('=')[1];
    var LigneStations = "";
    var nbrstation = 0;

    //Liste des Données:STATIONS
    for (i = 0; i < listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations.length; i++) {
        //Test de couleur de flag:
        if (IdChoixStations == 0) {
            if (listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].StatutCritique == "true")
                StatutDanger = "cancelled";
            else
                StatutDanger = "completed";
            nbrstation = nbrstation + 1;
            //Ligne de tableau
            ligne = '    ' +
                '                  <a href="#" class="project-block">' +
                '                     <!--<p class="project-type">Design</p>-->' +
                '                    <h5 class="project-name">' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '          <i onclick="window.location=\'PositionStationMap.html?lat=' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].x + '&lng=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].y + '&nameStation=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '\' " class="icon-map-pin"></i>     </h5>' +
                '                       <ul class="project-footer clearfix">' +
                '                          <li style="padding: 0px 0px 0px;">' +
                '                             <div class="stats-infos center-text">' +
                '                                <span class="text-default" style="font-size:12px;"> Pj Max</span>' +
                '                               <!--<i class="icon-users text-danger"></i>-->' +
                '                              <span class="badge badge-danger"> ' + listeAgences.agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].PjMax + '</span>' +
                '                         </div>' +
                '                    </li>' +
                '                   <li style="padding: 0px 0px 0px;">' +
                '                      <div class=" stats-infos center-text ">' +
                '                         <span class="text-default " style="font-size:12px; "> Pj Moy</span>' +
                '                        <!--<i class="icon-users text-danger "></i>-->' +
                '                       <span class="badge badge-info "> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMoy + ' </span>' +
                '                  </div>' +
                '             </li>' +
                '            <li style="padding: 0px 0px 0px;">' +
                '               <!--<div class="stats-infos"><i class="icon-line-graph text-info"></i> 29</div>-->' +
                '              <div class="stats-infos center-text">' +
                '                 <span class="text-default" style="font-size:12px;"> Pj T</span>' +
                '                <!--<i class="icon-users text-danger"></i>-->' +
                '               <span class="badge badge-success">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjT + ' </span>' +
                '          </div>' +
                '     </li>' +
                ' </ul>' +
                '<ul class="project-time clearfix">' +
                '   <li>' +
                '      <p>Q max</p>' +
                '     <span class="text-danger" style="font-size:20px;"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmax + ' </span>' +
                '</li>' +
                '             <li>' +
                '                <p>Q min</p>' +
                '               <span class="text-info" style="font-size:20px;">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmin + '</span>' +
                '          </li>' +
                '     </ul>' +
                '    <ul class="project-time clearfix">' +
                '                           <li>' +
                '                              <div class="sessions">' +
                '                                 <!--<h5 class="text-info">H Graph</h5>-->' +
                '                                <span class="text-default" style="font-size:12px;"> H Graph</span>' +
                '                               <div id="bouncerate" class="graph"></div>' +
                '                          </div>' +
                '                     </li>' +
                '                           <li>' +
                '                              <div class="sessions">' +
                '                                 <!--<h2 class="text-info">H Graph</h2>-->' +
                '                                <span class="text-default" style="font-size:12px;">Prev Graph</span>' +
                '                               <div id="duration" class="graph"></div>' +
                '                          </div>' +
                '                     </li>' +
                '                </ul>' +
                '                       <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>' +
                '                  </a>' +
                '           ';
            LigneStations = LigneStations + ligne;
        }

        if (IdChoixStations == 1) {
            if (listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].StatutCritique == "true") {
                StatutDanger = "cancelled";
                nbrstation = nbrstation + 1;
                //Ligne de tableau
                ligne = '    ' +
                    '                  <a href="#" class="project-block">' +
                    '                     <!--<p class="project-type">Design</p>-->' +
                    '                    <h5 class="project-name">' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '          <i onclick="window.location=\'PositionStationMap.html?lat=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].x + '&lng=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].y + '&nameStation=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '\' " class="icon-map-pin"></i>     </h5>' +
                    '                       <ul class="project-footer clearfix">' +
                    '                          <li style="padding: 0px 0px 0px;">' +
                    '                             <div class="stats-infos center-text">' +
                    '                                <span class="text-default" style="font-size:12px;"> Pj Max</span>' +
                    '                               <!--<i class="icon-users text-danger"></i>-->' +
                    '                              <span class="badge badge-danger"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMax + '</span>' +
                    '                         </div>' +
                    '                    </li>' +
                    '                   <li style="padding: 0px 0px 0px;">' +
                    '                      <div class=" stats-infos center-text ">' +
                    '                         <span class="text-default " style="font-size:12px; "> Pj Moy</span>' +
                    '                        <!--<i class="icon-users text-danger "></i>-->' +
                    '                       <span class="badge badge-info "> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMoy + ' </span>' +
                    '                  </div>' +
                    '             </li>' +
                    '            <li style="padding: 0px 0px 0px;">' +
                    '               <!--<div class="stats-infos"><i class="icon-line-graph text-info"></i> 29</div>-->' +
                    '              <div class="stats-infos center-text">' +
                    '                 <span class="text-default" style="font-size:12px;"> Pj T</span>' +
                    '                <!--<i class="icon-users text-danger"></i>-->' +
                    '               <span class="badge badge-success">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjT + ' </span>' +
                    '          </div>' +
                    '     </li>' +
                    ' </ul>' +
                    '<ul class="project-time clearfix">' +
                    '   <li>' +
                    '      <p>Q max</p>' +
                    '     <span class="text-danger" style="font-size:20px;"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmax + ' </span>' +
                    '</li>' +
                    '             <li>' +
                    '                <p>Q min</p>' +
                    '               <span class="text-info" style="font-size:20px;">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmin + '</span>' +
                    '          </li>' +
                    '     </ul>' +
                    '    <ul class="project-time clearfix">' +
                    '                           <li>' +
                    '                              <div class="sessions">' +
                    '                                 <!--<h5 class="text-info">H Graph</h5>-->' +
                    '                                <span class="text-default" style="font-size:12px;"> H Graph</span>' +
                    '                               <div id="bouncerate" class="graph"></div>' +
                    '                          </div>' +
                    '                     </li>' +
                    '                           <li>' +
                    '                              <div class="sessions">' +
                    '                                 <!--<h2 class="text-info">H Graph</h2>-->' +
                    '                                <span class="text-default" style="font-size:12px;">Prev Graph</span>' +
                    '                               <div id="duration" class="graph"></div>' +
                    '                          </div>' +
                    '                     </li>' +
                    '                </ul>' +
                    '                       <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>' +
                    '                  </a>' +
                    '           ';
                LigneStations = LigneStations + ligne;
            }
        }
        if (IdChoixStations == 2) {
            if (listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].StatutCritique == "false") {
                StatutDanger = "completed";
                nbrstation = nbrstation + 1;
                //Ligne de tableau
                ligne = '    ' +
                    '                  <a href="#" class="project-block">' +
                    '                     <!--<p class="project-type">Design</p>-->' +
                    '                    <h5 class="project-name">' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '          <i onclick="window.location=\'PositionStationMap.html?lat=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].x + '&lng=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].y + '&nameStation=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '\' " class="icon-map-pin"></i>     </h5>' +
                    '                       <ul class="project-footer clearfix">' +
                    '                          <li style="padding: 0px 0px 0px;">' +
                    '                             <div class="stats-infos center-text">' +
                    '                                <span class="text-default" style="font-size:12px;"> Pj Max</span>' +
                    '                               <!--<i class="icon-users text-danger"></i>-->' +
                    '                              <span class="badge badge-danger"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMax + '</span>' +
                    '                         </div>' +
                    '                    </li>' +
                    '                   <li style="padding: 0px 0px 0px;">' +
                    '                      <div class=" stats-infos center-text ">' +
                    '                         <span class="text-default " style="font-size:12px; "> Pj Moy</span>' +
                    '                        <!--<i class="icon-users text-danger "></i>-->' +
                    '                       <span class="badge badge-info "> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMoy + ' </span>' +
                    '                  </div>' +
                    '             </li>' +
                    '            <li style="padding: 0px 0px 0px;">' +
                    '               <!--<div class="stats-infos"><i class="icon-line-graph text-info"></i> 29</div>-->' +
                    '              <div class="stats-infos center-text">' +
                    '                 <span class="text-default" style="font-size:12px;"> Pj T</span>' +
                    '                <!--<i class="icon-users text-danger"></i>-->' +
                    '               <span class="badge badge-success">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjT + ' </span>' +
                    '          </div>' +
                    '     </li>' +
                    ' </ul>' +
                    '<ul class="project-time clearfix">' +
                    '   <li>' +
                    '      <p>Q max</p>' +
                    '     <span class="text-danger" style="font-size:20px;"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmax + ' </span>' +
                    '</li>' +
                    '             <li>' +
                    '                <p>Q min</p>' +
                    '               <span class="text-info" style="font-size:20px;">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmin + '</span>' +
                    '          </li>' +
                    '     </ul>' +
                    '    <ul class="project-time clearfix">' +
                    '                           <li>' +
                    '                              <div class="sessions">' +
                    '                                 <!--<h5 class="text-info">H Graph</h5>-->' +
                    '                                <span class="text-default" style="font-size:12px;"> H Graph</span>' +
                    '                               <div id="bouncerate" class="graph"></div>' +
                    '                          </div>' +
                    '                     </li>' +
                    '                           <li>' +
                    '                              <div class="sessions">' +
                    '                                 <!--<h2 class="text-info">H Graph</h2>-->' +
                    '                                <span class="text-default" style="font-size:12px;">Prev Graph</span>' +
                    '                               <div id="duration" class="graph"></div>' +
                    '                          </div>' +
                    '                     </li>' +
                    '                </ul>' +
                    '                       <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>' +
                    '                  </a>' +
                    '           ';
                LigneStations = LigneStations + ligne;
            }
        }


    }
    if (nbrstation == 0)
        LigneStations = '<h3> La liste des stations est vide</h3>';

    document.getElementById("dataStations").innerHTML = LigneStations;
}