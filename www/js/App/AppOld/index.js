/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
// var URLServeur = "http://beyond4edges.com";
var URLServeur = "http://indar.co";
// var URLServeur = "http://beyond4edges.com";


var app = {

    //URL Serveur:

    // Application Constructor
    initialize: function() {



        this.bindEvents();
        if (sessionStorage.getItem('listeagences') == null)
            app.GetData();

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    ChargementBassins: function() {


        // Variables:
        var PositionAgence = location.search.split('PositionAgence=')[1]; //"Agence2"; //"ABHBC";
        var res = sessionStorage.getItem('listeagences');
        var listeAgences = JSON.parse(res);
        var URLSousBassins = "ListeSousBassins.html";
        var ListeBassins = "";
        //var date1 = "" + document.getElementById("date1").value;
        //var date2 = "" + document.getElementById("date2").value;
        //alert(date1);
        // alert(date2);

        //--------Filtre des dates ------------------
        /*if (date1 && date2) {
                    alert(date1);
                    date1f = new Date(date1, "%Y-%m-%d");
                    alert(date1f);
                    alert(date1f.toLocaleFormat('%Y-%m-%d %H:%M:%S'));

                    var UrlBeyon = "http://www.beyond4edges.com/indar/indarstructure.php?fromDate=2017-01-27%2009:00:00&toDate=2017-01-27%2010:20:00";
                    $.ajax({
                        // url: URLServeur + FilePHPtoRUN,
                        url: UrlBeyon,
                        type: 'GET',
                        success: function(result) {
                            // alert("appel ws get data");
                            alert(result);
                            console.log(result);
                            // alert(result);
                            ObjListeAgances = result;

                            sessionStorage.setItem('listeagences', JSON.stringify(ObjListeAgances));
                            sessionStorage.setItem('flagconnexion', 'true');
                        }
                    });


            }*/
        //var usersession = sessionStorage.getItem('userSession');
        //var SESSIONINFO = JSON.parse(usersession);

        //Traitements
        // document.getElementById("usersession").innerHTML = SESSIONINFO.Username;
        var Tab = "";
        document.getElementById("NomAgence").innerHTML = "Agence : " + listeAgences.agences[PositionAgence].name;
        for (j = 0; j < listeAgences.agences[PositionAgence].bassins.length; j++) {



            Tab = Tab + '  <div class=\"col-md-6 col-lg-6 col-xl-3\" onclick="window.location=\'' + URLSousBassins + '?PositionAgence=' + PositionAgence + '&PositionBassin=' + j + '\';" >';
            Tab = Tab + "<section class=\"panel panel-featured-left panel-featured-primary\">";
            Tab = Tab + "<div class=\"panel-body\">";
            Tab = Tab + "<div class=\"widget-summary\">";
            Tab = Tab + "<div class=\"widget-summary-col widget-summary-col-icon\">";

            if (listeAgences.agences[PositionAgence].bassins[j].StatutCritique == "true") {
                Tab = Tab + "<div class=\"summary-icon bg-secondary\">";
            } else {

                Tab = Tab + "<div class=\"summary-icon bg-tertiary\">";
            }

            Tab = Tab + "<i class=\"fa fa-life-ring\"></i>";
            Tab = Tab + "</div>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"widget-summary-col\">";
            Tab = Tab + "<div class=\"summary\">";
            Tab = Tab + "<ul class=\"simple-bullet-list mb-xlg\">";
            Tab = Tab + "";
            Tab = Tab + "<span class=\"title\">" + listeAgences.agences[PositionAgence].bassins[j].name + "</span>";
            Tab = Tab + "</li>";
            Tab = Tab + "</ul>";
            Tab = Tab + "<div class=\"info\">";
            Tab = Tab + "<strong class=\"amount\">" + listeAgences.agences[PositionAgence].bassins[j].indicateurs.pluiemoy + "</strong>";
            Tab = Tab + "<span class=\"text-primary\">(Pluie Moy j en mm)</span>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"info\">";
            Tab = Tab + "<strong class=\"amount\">" + listeAgences.agences[PositionAgence].bassins[j].indicateurs.pluiemax + "</strong>";
            Tab = Tab + "<span class=\"text-primary\">(Pluie Max j en mm)</span>";
            Tab = Tab + "</div>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"summary-footer\">";

            Tab = Tab + "<a class=\"text-muted text-uppercase\"></a>";
            Tab = Tab + "</div></div></div></div></section></div> ";


        }
        document.getElementById("dataBassins").innerHTML = Tab;

        //Les données des indicateurs:
        /*var Indicateurs = '   <ul class="right-stats" style=" margin-top: 0px; margin-bottom: 0px;" id="mini-nav-right">' +
            '      <li><a href="javascript:void(0)" class="btn btn-info"><span>' + listeAgences.agences[PositionAgence].indicateurs.pluiemoy + '</span>Pluie Moy</a></li>' +
            '  <li><a href="#" class="btn btn-success"><span>' + listeAgences.agences[PositionAgence].indicateurs.pluiemax + '</span>Pluie MAX</a></li>' +

            '  </ul>';
        document.getElementById("indicateursData").innerHTML = Indicateurs;*/


    },
    ChargementSousBassins: function() {

        // Variables:
        var parametrestab = location.search.split('?'); //"Agence2"; //"ABHBC";
        var parametres = parametrestab[1].split('&');
        var posAg = parametres[0].split('=')[1];
        var posBas = parametres[1].split('=')[1];
        var URLStations = "ListeStations.html";
        var res = sessionStorage.getItem('listeagences');
        var listeAgences = JSON.parse(res);
        var LigneSousBassin = "";
        var Tab = "";
        //var usersession = sessionStorage.getItem('userSession');
        //var SESSIONINFO = JSON.parse(usersession);
        //Traitements
        //document.getElementById("usersession").innerHTML = SESSIONINFO.Username;
        //document.getElementById("NomBassin").innerHTML = "<h4>Agence : " + listeAgences.agences[posAg].name + "</h4><h5> Bassin : " + listeAgences.agences[posAg].bassins[posBas].name + "</h5>";
        for (j = 0; j < listeAgences.agences[posAg].bassins[posBas].SousBassins.length; j++) {




            Tab = Tab + '  <div class=\"col-md-6 col-lg-6 col-xl-3\" onclick="window.location=\'' + URLStations + '?PositionAgence=' + posAg + '&PositionBassin=' + posBas + '&PositionSousBassin=' + j + '\';"  >';
            Tab = Tab + "<section class=\"panel panel-featured-left panel-featured-primary\">";
            Tab = Tab + "<div class=\"panel-body\">";
            Tab = Tab + "<div class=\"widget-summary\">";
            Tab = Tab + "<div class=\"widget-summary-col widget-summary-col-icon\">";

            if (listeAgences.agences[posAg].bassins[posBas].StatutCritique == "true") {
                Tab = Tab + "<div class=\"summary-icon bg-secondary\">";
            } else {

                Tab = Tab + "<div class=\"summary-icon bg-tertiary\">";
            }

            Tab = Tab + "<i class=\"fa fa-life-ring\"></i>";
            Tab = Tab + "</div>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"widget-summary-col\">";
            Tab = Tab + "<div class=\"summary\">";
            Tab = Tab + "<ul class=\"simple-bullet-list mb-xlg\">";
            Tab = Tab + "";
            Tab = Tab + "<span class=\"title\">" + listeAgences.agences[posAg].bassins[posBas].SousBassins[j].name + "</span>";
            Tab = Tab + "</li>";
            Tab = Tab + "</ul>";
            Tab = Tab + "<div class=\"info\">";
            Tab = Tab + "<strong class=\"amount\">" + listeAgences.agences[posAg].bassins[posBas].SousBassins[j].indicateurs.pluiemoy + "</strong>";
            Tab = Tab + "<span class=\"text-primary\">(Pluie Moy j en mm)</span>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"info\">";
            Tab = Tab + "<strong class=\"amount\">" + listeAgences.agences[posAg].bassins[posBas].SousBassins[j].indicateurs.pluiemax + "</strong>";
            Tab = Tab + "<span class=\"text-primary\">(Pluie Max j en mm)</span>";
            Tab = Tab + "</div>";
            Tab = Tab + "</div>";
            Tab = Tab + "<div class=\"summary-footer\">";

            Tab = Tab + "<a class=\"text-muted text-uppercase\"></a>";
            Tab = Tab + "</div></div></div></div></section></div> ";




        }
        document.getElementById("dataSousBassins").innerHTML = Tab;

        //Les données des indicateurs:
        /*var Indicateurs = '   <ul class="right-stats" style="margin-top: 0px; margin-bottom: 0px;"id="mini-nav-right">' +
            '      <li><a href="javascript:void(0)" class="btn btn-info"><span>' + listeAgences.agences[posAg].bassins[posBas].indicateurs.pluiemoy + '</span>Pluie Moy</a></li>' +
            '  <li><a href="#" class="btn btn-success"><span>' + listeAgences.agences[posAg].bassins[posBas].indicateurs.pluiemax + '</span>Pluie MAX</a></li>' +

            '  </ul>';
        document.getElementById("indicateursData").innerHTML = Indicateurs;*/

    },
    ChargementRoutes: function() {



        button = "";
        StatutDanger = "cancelled";
        LigneStations = "";
        //Ligne de tableau



        ligne1 = '' +
            '<!--<p class="project-type">Design</p>-->' +
            ' <h5 class="project-name" > <p > Barrage Aaricha </p></h5>' +
            '                       <ul class="project-footer clearfix">' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Début</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 20-02-2017 10:23' + button + '</span>' +
            '                  </div>' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Fin</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 22-02-2017 16:45' + button + '</span>' +
            '                  </div>' +
            '             </li>' +
            ' </ul>' +
            '<ul class="project-timess clearfix">' +
            '<li>' +
            ' <div class="baguetteBoxThree gallery"><div class="col-lg-6 col-md-3 col-sm-3"><a style="text-align: center;" href="img/thumbs/lg-one.jpg" class="effects"><img src="img/thumbs/lg-one.jpg" class="img-responsive" alt="Arise Admin"><div class="overlay"><span class="expand">+</span></div></a></div></div> ' +
            '</li>' +
            '     </ul>';
        ligne2 = '' +
            '<!--<p class="project-type">Design</p>-->' +
            ' <h5 class="project-name" > <p > Mohammedia </p></h5>' +
            '                       <ul class="project-footer clearfix">' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Début</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 20-03-2017 23:20' + button + '</span>' +
            '                  </div>' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Fin</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 22-02-2017 00:23' + button + '</span>' +
            '                  </div>' +
            '             </li>' +
            ' </ul>' +
            '<ul class="project-timess clearfix">' +
            '<li>' +
            ' <div class="baguetteBoxThree gallery"><div class="col-lg-6 col-md-3 col-sm-3"><a style="text-align: center;" href="img/thumbs/lg-one.jpg" class="effects"><img src="img/thumbs/lg-one.jpg" class="img-responsive" alt="Arise Admin"><div class="overlay"><span class="expand">+</span></div></a></div></div> ' +
            '</li>' +
            '     </ul>';
        ligne3 = '' +
            '<!--<p class="project-type">Design</p>-->' +
            ' <h5 class="project-name" > <p > Barrage El Mellah  </p></h5>' +
            '                       <ul class="project-footer clearfix">' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Début</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 20-02-2017 12:00' + button + '</span>' +
            '                  </div>' +
            '                   <li style="padding: 0px 0px 0px;">' +
            '                      <div class=" stats-infos center-text ">' +
            '                         <span class="badge badge-info "> Date Fin</span> ' +
            '                        <!--<i class="icon-users text-danger "></i>-->' +
            '                     <span class="text-default " style="font-size:12px; "> 15-02-2017 14:45' + button + '</span>' +
            '                  </div>' +
            '             </li>' +
            ' </ul>' +
            '<ul class="project-timess clearfix">' +
            '<li>' +
            ' <div class="baguetteBoxThree gallery"><div class="col-lg-6 col-md-3 col-sm-3"><a style="text-align: center;" href="img/thumbs/lg-one.jpg" class="effects"><img src="img/thumbs/lg-one.jpg" class="img-responsive" alt="Arise Admin"><div class="overlay"><span class="expand">+</span></div></a></div></div> ' +
            '</li>' +
            '     </ul>';

        // ligne = ligne + ' <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>';

        LigneStations = LigneStations + ligne1;
        LigneStations = LigneStations + ligne2;
        LigneStations = LigneStations + ligne3;


        document.getElementById("dataRoutes").innerHTML = LigneStations;

        // TODO : Liste des Données:BARRAGES
        // for (i = 0; i < listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].Barrages.length; i++) {

        // }



    },
    ChargementAgences: function() {
        //alert("data agence");
        // alert(sessionStorage.getItem('listeagences'));
        // Variables:
        var res = sessionStorage.getItem('listeagences');
        console.log(res);
        var listeAgences = JSON.parse(res);
        var lignesAgences = "";
        var URLBassins = "ListeBassins.html";
        var usersession = sessionStorage.getItem('userSession');
        var SESSIONINFO = JSON.parse(usersession);
        //Traitements
        //document.getElementById("usersession").innerHTML = SESSIONINFO.Username;

        //Liste des Données:
        for (i = 0; i < listeAgences.agences.length; i++) {

            //Test de couleur de flag:

            if (listeAgences.agences[i].StatutCritique == "true")
                Couleur = "red";
            else
                Couleur = "green";
            //Ligne de tableau
            ligne = '<div class="col-md-4 col-sm-4 col-xs-16"> ' +
                '   <div class="users-wrapper red">' +
                ' <div class="users-info clearfix">' +
                '  <div class="users-avatar">' +
                '<i class="icon-flash icon-5x" style="color:' + Couleur + '"></i>' +
                '</div>' +
                '<div onclick="window.location=\'' + URLBassins + '?PositionAgence=' + i + '\';"  class="users-detail">' +
                '<h5>' + listeAgences.agences[i].name + '</h5>' +
                '<p>Responsable: ' + listeAgences.agences[i].Responsable + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 ui-sortable" style=" text-align: center;">' +
                '<span class="badge badge-info">Moy : ' + listeAgences.agences[i].indicateurs.pluiemoy +
                '</span> </div>' +
                '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 ui-sortable " style=" text-align: center;">' +
                '<span class="badge badge-success">Max : ' + listeAgences.agences[i].indicateurs.pluiemax +
                '</span> </div>' +
                '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 ui-sortable" style=" text-align: center;">' +
                '<p class="right-text">' +
                '<button type="button" class="btn btn-transparent" data-toggle="collapse" data-target="#A' + i + '"> ' +
                '<span class="badge badge-info"><span   class="icon-info"></span></span>  ' +
                '</button>' +
                '</p>' +
                '</div>' +
                '</div>' +
                '<div id="A' + i + '" class="collapse">' +
                '<ul class="users-footer clearfix">' +
                '<li>' +
                ' <p class="light">Emplacement</p>' +
                '<p>' + listeAgences.agences[i].Emplacement + '</p>' +
                '</li>' +
                '<li>' +
                '<p class="light">Contact</p>' +
                '<p>' + listeAgences.agences[i].Contact + '</p>' +
                '</li>' +
                '<li class="light"> </li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>';


            lignesAgences += ligne;

        }
        document.getElementById("dataAgences").innerHTML = lignesAgences;



    },
    /*
    ChargementStations: function() {

        var parametrestab = location.search.split('?'); //"Agence2"; //"ABHBC";
        var parametres = parametrestab[1].split('&');
        var posAg = parametres[0].split('=')[1];
        var posBas = parametres[1].split('=')[1];
        var posSousBas = parametres[2].split('=')[1];
        var res = sessionStorage.getItem('listeagences');
        var listeAgences = JSON.parse(res);
        var LigneStations = "";
        var usersession = sessionStorage.getItem('userSession');
        var SESSIONINFO = JSON.parse(usersession);
        //Traitements
        document.getElementById("usersession").innerHTML = SESSIONINFO.Username;

        //Liste des Données:STATIONS
        for (i = 0; i < listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations.length; i++) {
            //Test de couleur de flag:

            if (listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].StatutCritique == "true")
                StatutDanger = "cancelled";
            else
                StatutDanger = "completed";
            //Ligne de tableau
            ligne = '    ' +
                '                  <a href="#" class="project-block">' +
                '                     <!--<p class="project-type">Design</p>-->' +
                '                    <h5 class="project-name">' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].nom + '</h5>' +
                '                       <ul class="project-footer clearfix">' +
                '                          <li style="padding: 0px 0px 0px;">' +
                '                             <div class="stats-infos center-text">' +
                '                                <span class="text-default" style="font-size:12px;"> Pj Max</span>' +
                '                               <!--<i class="icon-users text-danger"></i>-->' +
                '                              <span class="badge badge-danger"> ' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].PjMax + '</span>' +
                '                         </div>' +
                '                    </li>' +
                '                   <li style="padding: 0px 0px 0px;">' +
                '                      <div class=" stats-infos center-text ">' +
                '                         <span class="text-default " style="font-size:12px; "> Pj Moy</span>' +
                '                        <!--<i class="icon-users text-danger "></i>-->' +
                '                       <span class="badge badge-info "> ' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].PjMoy + ' </span>' +
                '                  </div>' +
                '             </li>' +
                '            <li style="padding: 0px 0px 0px;">' +
                '               <!--<div class="stats-infos"><i class="icon-line-graph text-info"></i> 29</div>-->' +
                '              <div class="stats-infos center-text">' +
                '                 <span class="text-default" style="font-size:12px;"> Pj T</span>' +
                '                <!--<i class="icon-users text-danger"></i>-->' +
                '               <span class="badge badge-success">  ' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].PjT + ' </span>' +
                '          </div>' +
                '     </li>' +
                ' </ul>' +
                '<ul class="project-time clearfix">' +
                '   <li>' +
                '      <p>Q max</p>' +
                '     <span class="text-danger" style="font-size:20px;"> ' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].Qmax + ' </span>' +
                '</li>' +
                '             <li>' +
                '                <p>Q min</p>' +
                '               <span class="text-info" style="font-size:20px;">  ' + listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Stations[i].Qmin + '</span>' +
                '          </li>' +
                '     </ul>' +
                '    <ul class="project-time clearfix">' +
                '                           <li>' +
                '                              <div class="sessions">' +
                '                                 <!--<h5 class="text-info">H Graph</h5>-->' +
                '                                <span class="text-default" style="font-size:12px;"> H Graph</span>' +
                '                               <div id="bouncerate" class="graph"></div>' +
                '                          </div>' +
                '                     </li>' +
                '                           <li>' +
                '                              <div class="sessions">' +
                '                                 <!--<h2 class="text-info">H Graph</h2>-->' +
                '                                <span class="text-default" style="font-size:12px;">Prev Graph</span>' +
                '                               <div id="duration" class="graph"></div>' +
                '                          </div>' +
                '                     </li>' +
                '                </ul>' +
                '                       <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>' +
                '                  </a>' +
                '           ';
            LigneStations = LigneStations + ligne;

        }
        document.getElementById("dataStations").innerHTML = LigneStations;

        //Liste des Données:BARRAGES
        for (i = 0; i < listeAgences.Agences[posAg].Bassins[posBas].SousBassins[posSousBas].Barrages.length; i++) {

        }



    },
    */
    ChargementStations: function() {

        var parametrestab = location.search.split('?'); //"Agence2"; //"ABHBC";
        var parametres = parametrestab[1].split('&');
        var posAg = parametres[0].split('=')[1];
        var posBas = parametres[1].split('=')[1];
        var posSousBas = parametres[2].split('=')[1];
        var res = sessionStorage.getItem('listeagences');
        var listeAgences = JSON.parse(res);
        var LigneStations = "";
        //var usersession = sessionStorage.getItem('userSession');
        //var SESSIONINFO = JSON.parse(usersession);
        //Traitements
        //document.getElementById("usersession").innerHTML = SESSIONINFO.Username;

        //Liste des Données:STATIONS
        for (i = 0; i < listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations.length; i++) {
            //Test de couleur de flag:

            if (listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].StatutCritique == "true") {
                button = '<button type="button" Onclick="app.Modeliser()">Mod</button>';

            } else
                button = "";


            if (listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].StatutCritique == "true")
                StatutDanger = "cancelled";
            else
                StatutDanger = "completed";
            //Ligne de tableau
            ligne = '    ' +
                '                  <a href="#" class="project-block" style="padding-left: 0px;padding-right: 0px;">' +
                '                     <!--<p class="project-type">Design</p>-->' +
                '                    <h5 class="project-name" > <p id="namestationMod" >' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + ' </p>         <i onclick="window.location=\'PositionStationMap.html?lat=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].x + '&lng=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].y + '&nameStation=' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].name + '\' " class="el-icon-map-marker"></i> ds    </h5>' +
                '                       <ul class="project-footer clearfix">' +
                '                          <li style="padding: 0px 0px 0px;">' +
                '                             <div class="stats-infos center-text">' +
                '                                <span class="text-default" style="font-size:12px;"> Pj Max</span>' +
                '                               <!--<i class="icon-users text-danger"></i>-->' +
                '                              <span class="badge badge-danger"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMax + '</span>' +
                '                         </div>' +
                '                    </li>' +
                '                   <li style="padding: 0px 0px 0px;">' +
                '                      <div class=" stats-infos center-text ">' +
                '                         <span class="text-default " style="font-size:12px; "> Pj Moy' + button + '</span>' +
                '                        <!--<i class="icon-users text-danger "></i>-->' +
                '                       <span class="badge badge-info "> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMoy + ' </span>' +
                '                  </div>' +
                '             </li>' +
                '            <li style="padding: 0px 0px 0px;">' +
                '               <!--<div class="stats-infos"><i class="icon-line-graph text-info"></i> 29</div>-->' +
                '              <div class="stats-infos center-text">' +
                '                 <span class="text-default" style="font-size:12px;"> Pj T</span>' +
                '                <!--<i class="icon-users text-danger"></i>-->' +
                '               <span class="badge badge-success">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].PjMax + ' </span>' +
                '          </div>' +
                '     </li>' +
                ' </ul>' +
                '<ul class="project-time clearfix">' +
                '   <li>' +
                '      <p>Q max</p>' +
                '     <span class="text-danger" style="font-size:20px;"> ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmax + ' </span>' +
                '</li>' +
                '             <li>' +
                '                <p>Q min</p>' +
                '               <span class="text-info" style="font-size:20px;">  ' + listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].stations[i].Qmin + '</span>' +
                '          </li>' +
                '     </ul>';

            if (StatutDanger != "completed")
                ligne = ligne + '                       <div class="cancelled-project"></div><span class="project-btn ' + StatutDanger + ' badge badge-' + StatutDanger + '">Danger</span>';
            ligne = ligne + '                  </a>' +
                '           ';
            LigneStations = LigneStations + ligne;

        }
        document.getElementById("dataStations").innerHTML = LigneStations;

        // TODO : Liste des Données:BARRAGES
        // for (i = 0; i < listeAgences.agences[posAg].bassins[posBas].SousBassins[posSousBas].Barrages.length; i++) {

        // }



    },
    Login: function() {

        //Valeurs:
        var username = document.getElementById("Username").value;
        var password = document.getElementById("Password").value;

        // sessionStorage.clear();

        //Appel WS Login:
        var FilePHPtoRUN = "/Indar_V0_BackEnd/CheckLogin.php";
        var URLListeAgences = "ListeBassins.html?PositionAgence=0";
        var URLListeBassins = "http://indar.co" + "";
        var ErreurHTML = '<div class="alert alert-danger no-margin">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;' +
            '</button> <strong>Erreur!</strong> Le mot de passe ou l\'identifiant est incorrecte' +
            '</div>';

        sessionStorage.setItem('flagconnexion', 'false');
        $.ajax({
            url: "http://indar.co" + FilePHPtoRUN,
            type: 'GET',
            data: "username=" + username + "&password=" + password,
            success: function(result) {
                //console.log(result);
                var Response = JSON.parse(result);
                var statut = Response.Statut;
                var role = Response.Role;

                if (statut == "OK") {
                    sessionStorage.setItem('userSession', result);

                    if (role == "ResponsableAgences") {
                        //app.GetData();
                        window.location = URLListeAgences;
                    }
                } else {
                    document.getElementById("ErreurLogin").innerHTML = ErreurHTML;

                }

            },
            error: function(result, statut, erreur) {
                // alert(result);

            }
        });

    },
    Logout: function() {

        var URLLogin = "index.html";
        sessionStorage.clear();
        window.location = URLLogin;


    },
    GetData: function() {
        //Récuperer liste des responsables et les agences:
        // var FilePHPtoRUN = "/indar/indarstructure.php";

        //alert("this is getdate");
        var UrlBeyon = "http://www.beyond4edges.com/indar/indarstructure.php?fromDate=2017-01-27%2009:00:00&toDate=2017-01-27%2010:20:00";
        $.ajax({
            // url: URLServeur + FilePHPtoRUN,
            url: UrlBeyon,
            type: 'GET',
            success: function(result) {
                // alert("appel ws get data");
                //  alert(result);
                console.log(result);
                // alert(result);
                ObjListeAgances = result;

                sessionStorage.setItem('listeagences', JSON.stringify(ObjListeAgances));
                sessionStorage.setItem('flagconnexion', 'true');
            }
        });

        // alert(sessionStorage.getItem('listeagences'));
    },
    splitLink: function() {
        var param = location.search.substring(1).split("&");
        //alert (param);

        var temp = param[0].split("=");
        id = unescape(temp[1]);
        temp = param[1].split("=");
        img = unescape(temp[1]);
        var obj = document.getElementById('imgHec');
        obj.setAttribute('href', 'imgs/' + img);

        obj = document.getElementById('imgHec2');
        obj.setAttribute('src', 'imgs/' + img);




    },
    Modeliser: function() {
        //alert("Hello Modelisation");
        var URLServeur = "http://localhost";
        var URLModelisation = "/indarTest24h/ResultatModel/Modeliser.php"
        var nameStation = document.getElementById("namestationMod").innerHTML;
        $.ajax({
            url: URLServeur + URLModelisation,
            type: 'POST',
            data: "station=" + nameStation,
            success: function(result) {

                // alert(result);
            },
            error: function(result, statut, erreur) {
                alert("Error : Try Again!");

            }
        });

    }
};

//alert(sessionStorage.getItem('listeagences'));
//if (sessionStorage.getItem('listeagences') == null)
app.initialize();