/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var URLServeur = "http://193.34.145.111";
var URLGraphes = "/Indar/ResultatModel/Graphes/";

var app = {

    //URL Serveur:

    // Application Constructor
    initialize: function() {



        this.bindEvents();
/*
        //Récuperer liste des responsables et les agences:
        var FilePHPtoRUN = "/Indar/ResultatModel/Readfiles.php";


        $.ajax({
            url: URLServeur + FilePHPtoRUN,
            success: function(result) {


                listeresultats = "";

                var liste = JSON.parse(result);
                for (j = liste.length - 1; j >= 0; j--) {


                    var btn2 = "<a href=\"DetailsModelisation.html?img=" + URLServeur + URLGraphes + liste[j].Grapgename + "\"> <button class=\"mb-xs mt-xs mr-xs btn btn-primary\" type=\"button\">i</button> </a>";
                    btn2 = "<a href=\"DetailsModelisation.html?img=" + URLServeur + URLGraphes + liste[j].Grapgename + "\"> <span class=\"label label-info\">i</span> </a>";
                    //btn2 ='<span class="label label-info">i</span>';
                    var datee = new Date(liste[j].date + " " + liste[j].time.replace("-", ":"));

                    var day = datee.getDate();
                    var monthIndex = datee.getMonth();
                    var year = datee.getFullYear();

                    var concatDate = day + '/' + monthIndex+ '/' + year;
                    var typem = "";
                    if (liste[j].type == "P")
                        typem = "Prédictions"
                    if (liste[j].type == "R")
                        typem = "Temps réel"


                    var alertLevel = '';
                    if(Math.floor(Math.random() * Math.floor(100))%2)
                    {
                        alertLevel ='<span class="label label-danger">Danger</span>';
                    }else{
                        alertLevel ='<span class="label label-warning">Warning</span>';
                    }
                    ligne = '   <tr class="gradeX">' +
                        '      <td>' + concatDate + '</td>' +
                        '      <td>' + typem + '</td>' +
                        '    <td>' + liste[j].name + '</td>' +
                        '    <td>' + alertLevel + '</td>' +
                        '  <td>' + btn2 + '</td>' +
                        //  '  <td> <i onclick="window.location=\'PositionStationMap.html?lat=33,533285&lng=-7,583324&nameStation=maStation\' " class="el-icon-map-marker"></i> </td>' +
                        '</td>' +
                        '</tr>';

                    listeresultats = listeresultats + ligne;
                }




                document.getElementById("listeresultats").innerHTML = listeresultats;
            }
        });
*/
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    Logout: function() {

        var URLLogin = "index.html";
        localStorage.clear();
        window.location = URLLogin;


    }
};

app.initialize();