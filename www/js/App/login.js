function login() {
    var username = document.getElementById("Username").value;
    var password = document.getElementById("Password").value;

    // sessionStorage.clear();

    //Appel WS Login:
    var FilePHPtoRUN = "/Indar_V0_BackEnd/CheckLogin.php";
    var URLListeAgences = "ListeAgences.html";
    var URLListeBassins = "http://indar.co" + "";
    var ErreurHTML = '<div class="alert alert-danger no-margin">' +
        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;' +
        '</button> <strong>Erreur!</strong> Le mot de passe ou l\'identifiant est incorrecte' +
        '</div>';

    sessionStorage.setItem('flagconnexion', 'false');
    $.ajax({
        url: "http://indar.co" + FilePHPtoRUN,
        type: 'GET',
        data: "username=" + username + "&password=" + password,
        success: function(result) {
            //console.log(result);
            var Response = JSON.parse(result);
            var statut = Response.Statut;
            var role = Response.Role;

            if (statut == "OK") {
                sessionStorage.setItem('userSession', result);

                if (role == "ResponsableAgences") {
                    //app.GetData();
                    alert(URLListeAgences);
                    window.location = URLListeAgences;
                }
            } else {
                document.getElementById("ErreurLogin").innerHTML = ErreurHTML;

            }

        },
        error: function(result, statut, erreur) {
            // alert(result);

        }
    });



}